package com.codecool.moviewatcher.dto.mappers;

import com.codecool.moviewatcher.auth.User;
import com.codecool.moviewatcher.dto.MovieDto;
import com.codecool.moviewatcher.model.Movie;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-22T09:00:25+0700",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl implements MovieMapper {

    @Override
    public Movie movieDtoToMovie(MovieDto movieDto) {
        if ( movieDto == null ) {
            return null;
        }

        Movie movie = new Movie();

        movie.setId( movieDto.getId() );
        movie.setTitle( movieDto.getTitle() );
        movie.setVoteAverage( movieDto.getVoteAverage() );
        movie.setReleaseDate( movieDto.getReleaseDate() );
        movie.setPosterPath( movieDto.getPosterPath() );
        Set<User> set = movieDto.getLikedBy();
        if ( set != null ) {
            movie.setLikedBy( new HashSet<User>( set ) );
        }
        Set<User> set1 = movieDto.getWatchlistedBy();
        if ( set1 != null ) {
            movie.setWatchlistedBy( new HashSet<User>( set1 ) );
        }

        return movie;
    }

    @Override
    public MovieDto movieToMovieDto(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDto movieDto = new MovieDto();

        movieDto.setId( movie.getId() );
        movieDto.setTitle( movie.getTitle() );
        movieDto.setVoteAverage( movie.getVoteAverage() );
        movieDto.setReleaseDate( movie.getReleaseDate() );
        movieDto.setPosterPath( movie.getPosterPath() );
        Set<User> set = movie.getLikedBy();
        if ( set != null ) {
            movieDto.setLikedBy( new HashSet<User>( set ) );
        }
        Set<User> set1 = movie.getWatchlistedBy();
        if ( set1 != null ) {
            movieDto.setWatchlistedBy( new HashSet<User>( set1 ) );
        }

        return movieDto;
    }

    @Override
    public List<MovieDto> movieListToMovieDtoList(List<Movie> userList) {
        if ( userList == null ) {
            return null;
        }

        List<MovieDto> list = new ArrayList<MovieDto>( userList.size() );
        for ( Movie movie : userList ) {
            list.add( movieToMovieDto( movie ) );
        }

        return list;
    }
}
