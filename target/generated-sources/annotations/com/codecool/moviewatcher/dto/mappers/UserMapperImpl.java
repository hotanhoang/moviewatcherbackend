package com.codecool.moviewatcher.dto.mappers;

import com.codecool.moviewatcher.auth.User;
import com.codecool.moviewatcher.dto.MovieDto;
import com.codecool.moviewatcher.dto.UserDto;
import com.codecool.moviewatcher.model.Movie;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-22T09:00:25+0700",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto userToUserDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( user.getId() );
        userDto.setEmail( user.getEmail() );
        userDto.setPassword( user.getPassword() );
        userDto.setCreatedDate( user.getCreatedDate() );
        userDto.setFavorites( movieListToMovieDtoList( user.getFavorites() ) );
        userDto.setWatchlist( movieListToMovieDtoList( user.getWatchlist() ) );

        return userDto;
    }

    protected MovieDto movieToMovieDto(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDto movieDto = new MovieDto();

        movieDto.setId( movie.getId() );
        movieDto.setTitle( movie.getTitle() );
        movieDto.setVoteAverage( movie.getVoteAverage() );
        movieDto.setReleaseDate( movie.getReleaseDate() );
        movieDto.setPosterPath( movie.getPosterPath() );
        Set<User> set = movie.getLikedBy();
        if ( set != null ) {
            movieDto.setLikedBy( new HashSet<User>( set ) );
        }
        Set<User> set1 = movie.getWatchlistedBy();
        if ( set1 != null ) {
            movieDto.setWatchlistedBy( new HashSet<User>( set1 ) );
        }

        return movieDto;
    }

    protected List<MovieDto> movieListToMovieDtoList(List<Movie> list) {
        if ( list == null ) {
            return null;
        }

        List<MovieDto> list1 = new ArrayList<MovieDto>( list.size() );
        for ( Movie movie : list ) {
            list1.add( movieToMovieDto( movie ) );
        }

        return list1;
    }
}
